package org.mipt.annotation_processing;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.ExecutableType;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedAnnotationTypes("org.mipt.annotation_processing.BuilderProperty")
public class BuilderProcessor extends AbstractProcessor {
    private void messageForInvalid(List<Element> invalid){
        for(Element el: invalid){
            processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "Error applying annotation", el);
        }
    }

    private List<Element> retrieveSetters(TypeElement annotation, RoundEnvironment roundEnv){
        Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(annotation);
        Map<Boolean, List<Element>> methods = elements.stream().collect(
                Collectors.partitioningBy(element ->
                        ((ExecutableType) element.asType()).getParameterTypes().size() == 1 &&
                                element.getSimpleName().toString().startsWith("set")
                )
        );
        messageForInvalid(methods.get(false));
        return methods.get(true);
    }

    private Map<String, String > createParametersMapping(List<Element> setters){
        Map<String, String> setterParameters = new HashMap<>();
        for(Element setter: setters){
            setterParameters.put(setter.getSimpleName().toString(),
                    ((ExecutableType) setter.asType()).getParameterTypes().get(0).toString());
        }
        return setterParameters;
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        for(TypeElement annotation: annotations){
            List<Element> setters = retrieveSetters(annotation, roundEnv);
            if(setters.isEmpty()){
                continue;
            }
            String className = ((TypeElement) setters.get(0).getEnclosingElement()).getQualifiedName().toString();
            Map<String, String> setterParameters = createParametersMapping(setters);

            try {
                writeBuilderFile(className, setterParameters);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    private void writeBuilderFile(String className, Map<String, String> setterMap) throws IOException {
        String packageName = null;
        int lastDot = className.lastIndexOf('.');
        if (lastDot > 0) {
            packageName = className.substring(0, lastDot);
        }

        String simpleClassName = className.substring(lastDot + 1);
        String builderClassName = className + "Builder";
        String builderSimpleClassName = builderClassName.substring(lastDot + 1);

        JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(builderClassName);
        try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {

            if (packageName != null) {
                out.print("package ");
                out.print(packageName);
                out.println(";");
                out.println();
            }

            out.print("public class ");
            out.print(builderSimpleClassName);
            out.println(" {");
            out.println();

            out.print("    private ");
            out.print(simpleClassName);
            out.print(" object = new ");
            out.print(simpleClassName);
            out.println("();");
            out.println();

            out.print("    public ");
            out.print(simpleClassName);
            out.println(" build() {");
            out.println("        return object;");
            out.println("    }");
            out.println();

            setterMap.entrySet().forEach(setter -> {
                String methodName = setter.getKey();
                String argumentType = setter.getValue();

                out.print("    public ");
                out.print(builderSimpleClassName);
                out.print(" ");
                out.print(methodName);

                out.print("(");

                out.print(argumentType);
                out.println(" value) {");
                out.print("        object.");
                out.print(methodName);
                out.println("(value);");
                out.println("        return this;");
                out.println("    }");
                out.println();
            });

            out.println("}");

        }
    }

//    Person person = new PersonBuilder()
//            .setAge(25)
//            .setName("John")
//            .build();
}
