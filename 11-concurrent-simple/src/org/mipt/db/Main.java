package org.mipt.db;

public class Main {
    /**
     * Creates the specified number of readers and writers and starts them.
     */
    public static void main(String[] args) {
        DataBaseImpl db = new DataBaseImpl();

        ReaderImpl readerImpl = new ReaderImpl(db);
        ReaderImpl readerImpl1 = new ReaderImpl(db);
        ReaderImpl readerImpl2 = new ReaderImpl(db);

        WriterImpl writerImpl = new WriterImpl(db);

        System.out.println("run readers");

        readerImpl.start();
        readerImpl1.start();
        readerImpl2.start();

        System.out.println("run writer");
        writerImpl.start();

    }
}
