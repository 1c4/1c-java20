package org.mipt.db;

public class WriterImpl extends Thread {
    private static int writers = 0;
    private int number;
    private DataBase database;
    /**
     * Creates a WriterImpl for the specified database.
     *
     * @param database database to which to write.
     */
    public WriterImpl(DataBaseImpl database) {
        this.number = WriterImpl.writers++;
        this.database = database;
    }

    /**
     * Writes.
     */
    @Override
    public void run() {
        try {
            Thread.sleep((long)(Math.random() * DataBaseImpl.SLEEPING_DELAY));
        } catch (InterruptedException e) {
        }
        database.write(number);
    }
}
