package org.mipt.db;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DataBaseImpl implements DataBase{
    public static final int SLEEPING_DELAY = 5000;
    private int readers;

    public DataBaseImpl() {
        this.readers = 0;
    }

    public static String getTime(){
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss.SSS");
        return dateFormat.format(date);
    }

    @Override
    public void read(int number) {
        synchronized (this) {
            readers++;
            System.out.println("Reader" + number + "starts" + getTime());
        }
        try {
            Thread.sleep((long)(Math.random() * SLEEPING_DELAY));
        }
        catch (InterruptedException e){}
        synchronized (this){
            System.out.println("Reader" + number + "stops" + getTime());
            readers--;
            if(readers == 0){
                this.notifyAll();
            }
        }
    }

    @Override
    public synchronized void write(int number) {
        while (readers != 0){
            try {
                this.wait();
            } catch (InterruptedException e) {}
        }
        System.out.println("Writer starts" + getTime());
        try {
            Thread.sleep((long)(Math.random() * SLEEPING_DELAY));
        }
        catch (InterruptedException e){}
        System.out.println("Writer stops" + getTime());
        this.notifyAll();
    }
}
