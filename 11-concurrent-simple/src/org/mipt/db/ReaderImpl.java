package org.mipt.db;

public class ReaderImpl extends Thread {
    private static int readers = 0;
    private int number;
    private DataBase database;
    /**
     * Creates a ReaderImpl for the specified database.
     *
     * @param database database from which to be read.
     */
    public ReaderImpl(DataBaseImpl database) {
        this.number = ReaderImpl.readers++;
        this.database = database;
    }

    /**
     * Reads.
     */
    @Override
    public void run() {
        try {
            Thread.sleep((long)(Math.random() * DataBaseImpl.SLEEPING_DELAY));
        } catch (InterruptedException e) {
        }
        database.read(number);
    }
}
