package org.mipt.egg_chicken;

public class Main {

    private static EggVoice eggOpinion;
    private static ChickenVoice chickenOpinion;

    public static void main(String[] args) throws InterruptedException {
        eggOpinion = new EggVoice();
        chickenOpinion = new ChickenVoice(eggOpinion);

        System.out.println("Debate starts...");

        eggOpinion.start();
        chickenOpinion.start();

        eggOpinion.join();
        chickenOpinion.join();

        System.out.println("Debate is over!");
    }
}
