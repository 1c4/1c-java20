package org.mipt.egg_chicken;

public class ChickenVoice extends Thread {
    EggVoice eggVoice;

    ChickenVoice(EggVoice eggVoice) {
        this.eggVoice = eggVoice;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            try {
                sleep(500);
            }
            catch (InterruptedException e) {
            }

            System.out.println("Chicken!!");
        }

        if (eggVoice.isAlive())    //Если оппонент еще не сказал последнее слово
        {
            try {
                eggVoice.join();    //Подождать пока оппонент закончит высказываться.
            }
            catch (InterruptedException e) {
            }

            System.out.println("Egg was the first!");
        }
        else    //если оппонент уже закончил высказываться
            System.out.println("Chicken was the first!");
    }
}

