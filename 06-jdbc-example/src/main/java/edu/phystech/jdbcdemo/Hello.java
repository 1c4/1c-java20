package edu.phystech.jdbcdemo;

import edu.phystech.jdbcdemo.db.ConnectionSource;
import org.h2.jdbcx.JdbcConnectionPool;

import java.io.IOException;
import java.sql.*;

public class Hello {


    public static void main(String[] args) throws SQLException, IOException {


        ConnectionSource source = new ConnectionSource(JdbcConnectionPool.create("sss", "", ""));

        Connection connection1 = source.getConnection();

        int count = source.statement(stmt -> {
            ResultSet resultSet = stmt.executeQuery("select count * from smth");
            resultSet.next();
            return resultSet.getInt(1);
        });
    }

}
