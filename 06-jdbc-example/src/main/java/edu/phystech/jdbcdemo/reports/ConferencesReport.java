package edu.phystech.jdbcdemo.reports;

import edu.phystech.jdbcdemo.dao.ConferenceDao;
import edu.phystech.jdbcdemo.db.ConnectionSource;
import edu.phystech.jdbcdemo.db.DbInit;
import edu.phystech.jdbcdemo.domain.Conference;
import org.apache.poi.ss.usermodel.Color;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.usermodel.*;
import org.h2.jdbcx.JdbcConnectionPool;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;

public class ConferencesReport {
    public static class TestData{
        public final static Conference JPOINT = new Conference(1, "JPoint");
        public final static Conference JOKER = new Conference(2, "Joker");
    }

    private static ConnectionSource source = new ConnectionSource(
            JdbcConnectionPool.create("jdbc:h2:mem:database;DB_CLOSE_DELAY=-1",
                    "", ""));
    private static ConferenceDao dao =
            new ConferenceDao(source);

    private static void setupDB() throws IOException, SQLException {
        new DbInit(source).create();
        dao.saveConferences(Arrays.asList(TestData.JPOINT, TestData.JOKER));
    }

    private static String generateReport(Collection<Conference> conferences) throws IOException, IllegalAccessException {
        StringBuilder sb = new StringBuilder("report_");
        sb.append(System.currentTimeMillis());
        sb.append(".xlsx");
        String filename = sb.toString();
        FileOutputStream fileOutputStream = new FileOutputStream(filename);
        XSSFWorkbook report = new XSSFWorkbook();
        XSSFSheet sheet = report.createSheet("conferences");

        XSSFRow row = null;
        XSSFCell cell = null;

        Field[] fields = Conference.class.getDeclaredFields();
        row = sheet.createRow(0);
        for(int i=0; i<fields.length; i++){
            cell = row.createCell(i);
            XSSFCellStyle headerStyle = report.createCellStyle();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            headerStyle.setFillForegroundColor((short)128);
            cell.setCellStyle(headerStyle);
            cell.setCellValue(fields[i].getName());
            sheet.createFreezePane(0, 1);
        }

        for(Field f: fields){
            f.setAccessible(true);
        }
        int cnt = 1;
        for(Conference conference: conferences){
            row = sheet.createRow(cnt);

            for(int i=0; i<fields.length; i++){
                cell = row.createCell(i);
                if(fields[i].getType() == String.class) {
                    cell.setCellValue((String) fields[i].get(conference));
                }
                else if (fields[i].getType() == int.class){
                    cell.setCellValue((Integer) fields[i].get(conference));
                }
            }
            cnt++;
        }
        report.write(fileOutputStream);
        return filename;
    }

    public static void autoOpen(String filename){
        String cmdHeader = null;
        try {
            if(System.getProperty("os.name").contains("Linux")){
                cmdHeader = "libreoffice ";
            }
            else {
                cmdHeader = "cmd /c start ";
            }
            Process process = Runtime.getRuntime().exec(cmdHeader + filename);
        }catch (IOException e){
            System.err.println("unable to open file" + filename);
        }
    }

    public static void reflectionTest() throws IllegalAccessException, InvocationTargetException {
//        Field[] fields = Conference.class.getDeclaredFields();
//        for(Field f: fields){
//            System.out.println(f.getName());
//            f.setAccessible(true);
//            System.out.println(f.get(TestData.JOKER));
//            System.out.println(f.getType());
//            f.setAccessible(false);
//        }
        Method[] methods = Conference.class.getMethods();
        for(Method m: methods){
            if(m.getDeclaringClass() == Conference.class){
                System.out.println(m.getName());
                System.out.println(m.invoke(TestData.JOKER, TestData.JPOINT));
            }
        }
    }

    public static void main(String[] args) throws SQLException, IOException, IllegalAccessException, InvocationTargetException {
//        setupDB();
//        String repostName = generateReport(dao.getConferences());
//        autoOpen(repostName);
        reflectionTest();
    }
}
