package org.mipt;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {
    private int field = 3;

    public static void test1() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new LinkedList<>();
        System.out.println(list1.getClass().getSimpleName() + " " + list2.getClass().getSimpleName());
        Class<? extends List> cl = list1.getClass();
        List<Integer> list3 = cl.getDeclaredConstructor().newInstance();
        System.out.println(list3.getClass().getCanonicalName());
    }

    public static void tetstLiterals() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<? extends Main> mainInfo = Main.class;
//        System.out.println(mainInfo.getCanonicalName());
        Class<? extends Integer> integerClass = Integer.class;
        Class<? extends Integer> intClass = int.class;
//        System.out.println(intClass.getCanonicalName());
        Integer res = integerClass.getDeclaredConstructor(String.class).newInstance("5");
        int res2 = intClass.getDeclaredConstructor(int.class).newInstance(5);
        Class<? extends int[]> massClass = int[].class;
//        for(Constructor c: integerClass.getDeclaredConstructors()){
//            System.out.println(c.getName());
//        }
//        System.out.println(result);
    }

    public static void testClassLoader() throws ClassNotFoundException {
        Class<?> clazz = Class.forName("org.mipt.Main");
        System.out.println(clazz.getDeclaredMethods().length);
    }

    public static void invokeMethod() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class<?> clazz = Class.forName("org.mipt.Main");
        Method m = clazz.getDeclaredMethod("testClassLoader");
        m.invoke(new Main());
    }

    public static void changeField() throws ClassNotFoundException, IllegalAccessException {
        Class<?> clazz = Class.forName("org.mipt.Main");
        Main testObj = new Main();
        for(Field f: clazz.getDeclaredFields()){
//            f.setAccessible(true);
            System.out.println(f.getInt(testObj));
            f.set(testObj, 10);
            System.out.println(f.getInt(testObj));
        }
    }

    public static void main(String[] args)
            throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        test1();
        System.out.println(new Main().getClass().getCanonicalName());
        tetstLiterals();
        testClassLoader();
        invokeMethod();
        changeField();
    }
}
