package org.mipt;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SimpleExecutors {
    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(() -> {
            String threadName = Thread.currentThread().getName();
            System.out.println("Hello, I'm " + threadName);
            try {
                TimeUnit.SECONDS.sleep(50);
            } catch (InterruptedException e) {
                System.out.println("I'm interrupted :(");
            }
        });
        executorService.shutdown();
        executorService.awaitTermination(2, TimeUnit.SECONDS);
        if(!executorService.isTerminated()){
            executorService.shutdownNow();
        }
    }
}
