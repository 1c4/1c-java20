package org.mipt;

import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

public class AtomicExamples {
    public static Random random = new Random();
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        AtomicInteger atomicInteger = new AtomicInteger(0);
        ExecutorService executor = Executors.newFixedThreadPool(5);

        Runnable task = new Runnable() {
            @Override
            public void run() {
//                atomicInteger.incrementAndGet();
                try {
                    TimeUnit.MILLISECONDS.sleep(random.nextInt(5000));
                    atomicInteger.updateAndGet(n -> n + 5);
                } catch (InterruptedException e) {
                    System.out.println("Interrupted: " + Thread.currentThread().getName());
                }
            }
        };

        Future<?> res = null;
        for(int i = 0; i < 100; i++){
            res = executor.submit(task);
        }
//        IntStream.range(0, 1000).forEach(i -> {
//            executor.submit(task);
//        });
        res.get();
        executor.shutdown();
        System.out.println(atomicInteger.get());
    }
}
