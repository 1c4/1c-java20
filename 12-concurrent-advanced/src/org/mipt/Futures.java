package org.mipt;

import java.util.concurrent.*;

public class Futures {
    public static void main(String[] args) throws ExecutionException, InterruptedException{
        Callable<Integer> task = () -> {
            int sleepingTime = 5;
            TimeUnit.SECONDS.sleep(sleepingTime);
            return sleepingTime;
        };

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<Integer> resultFuture = executor.submit(task);
        System.out.println("Is done? \n -" + resultFuture.isDone());
        int result = 0;
        try {
            result = resultFuture.get(1, TimeUnit.SECONDS);
        } catch (TimeoutException e) {
            System.out.println("Too early!");
        }
        System.out.println("Is done? \n -" + resultFuture.isDone());
        TimeUnit.SECONDS.sleep(10);
        System.out.println("Is done? \n -" + resultFuture.isDone());
        result = resultFuture.get();
        int result2 = resultFuture.get();
        System.out.println("Result is: " + result);
        executor.shutdown();
    }
}
