package org.mipt;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Scheduling {
    private static void simpleScheduling() throws InterruptedException {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        Runnable task = new Runnable() {
            @Override
            public void run() {
                System.out.println("Scheduled at " + System.currentTimeMillis());
            }
        };

        TimeUnit.SECONDS.sleep(1);
        ScheduledFuture<?> future = executor.schedule(task, 3, TimeUnit.SECONDS);
        long remainigDelay = future.getDelay(TimeUnit.SECONDS);
        System.out.println("Remainig delay: " + remainigDelay);

        executor.shutdown();
        executor.awaitTermination(10, TimeUnit.SECONDS);
        if(!executor.isTerminated()){
            executor.shutdownNow();
        }
    }

    private static void advancedScheduling() throws InterruptedException {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(5);
        Runnable task = new Runnable() {
            @Override
            public void run() {
                try {
                    DateTimeFormatter df = DateTimeFormatter.ofPattern("HH:mm:ss");
                    System.out.println("Scheduled at " + df.format(LocalDateTime.now()));
                    TimeUnit.SECONDS.sleep(10);
                    System.out.println("Finished at " + df.format(LocalDateTime.now()));
                } catch (InterruptedException e) {
                    System.out.println("Interrupted :(");
                }
            }
        };
//        ScheduledFuture<?> future = executor.scheduleAtFixedRate(task, 0, 1, TimeUnit.SECONDS);
        ScheduledFuture<?> future = executor.scheduleWithFixedDelay(task, 0, 1, TimeUnit.SECONDS);
        TimeUnit.SECONDS.sleep(1);
        ScheduledFuture<?> future2 = executor.scheduleWithFixedDelay(task, 0, 1, TimeUnit.SECONDS);
//        executor.shutdown();
    }

    public static void main(String[] args) throws InterruptedException {
        advancedScheduling();
    }
}
