Maven - инструмент автоматической сборки проектов. Необходим для компиляции всех файлов по всем директориям проекта, 
указания зависимостей на сторонние библиотеки.

#### Жизненный цикл Maven
Сборка maven соответствует специфическому жизненному циклу для развертывания и распространения проекта.
Выделяют три встроенных жизненных цикла:
* default: основной жизненный цикл, ответственный за развертывание проекта
* clean: чиста проекта и удаление всех файлов, сгенерированных в предыдущую сборку
* site: создание проектной документации

#### Фазы Maven

Фазы представляют шаги в жизненном цикле maven:

* validate — проверяет корректность метаинформации о проекте
* compile — компилирует исходники
* test — прогоняет тесты классов из предыдущего шага
* package — упаковывает скомпилированые классы в удобноперемещаемый формат (jar или war, к примеру)
* integration-test — отправляет упаковынные классы в среду интеграционного тестирования и прогоняет тесты
* verify — проверяет корректность пакета и удовлетворение требованиям качества
* install — загоняет пакет в локальный репозиторий, откуда он (пекат) будет доступен для использования как зависимость в других проектах
* deploy — отправляет пакет на удаленный production сервер, откуда другие разработчики его могут получить и использовать

При этом все шаги последовательны. И если, к примеру, выполнить $ mvn package, то фактически будут выполнены шаги: validate, compile, test и package. 
За более подробной информацией можно обратиться к официальной докумен ации Maven - http://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html#Lifecycle_Reference

#### POM file
Вся структура проекта описывается в pom файле. Он строится в формате xml.
Ключевым понятием Maven является артефакт — это любая библиотека, хранящаяся в репозитории. Это может быть какая-то зависимость или плагин. 
* Зависимости — это те библиотеки, которые непосредственно используются в вашем проекте для компиляции кода или его тестирования.
* Плагины используются самим Maven'ом при сборке проекта или для каких-то других целей (деплоймент, создание файлов проекта для IDEA и др.).
* Архетип — это некая стандартная компоновка файлов и каталогов в проектах различного рода (веб, swing-проекты и прочие). 
 Другими словами, Maven знает, как обычно строятся проекты и в соответствии с архетипом создает структуру каталогов.

#### Установка
На windows-системах maven придется устанавливать вручную. Последнюю версию maven'a можно скачать на официальной странице - http://maven.apache.org/download.cgi
Просто распаковываем архив в любую директорию. Далее необходимо создать переменную в Path, в которой необходимо указать путь к Maven.
Заходим в Win + Pause – Дополнительно – Переменные среды – в верхнем окошке нажимаем Создать, вводим имя M2_HOME и значение допустим “C:\apache-maven-2.2.1”. 
Далее там же создаем еще одну переменную M2 со значением %M2_HOME%\bin. Так же убеждаемся, что есть переменная JAVA_HOME с путем к JDK. 
Ее значение должно быть примерно таким «c:\Program Files\Java\jdk1.6.0_10\». И наконец в том же окошке создаем/модифицируем переменную Path, в нее необходимо просто написать 
%M2%, чтобы наша папочка с исполняемым файлом Maven была видна из командной строки. 

Проверить рабостоспособность можно команой ```mvn –version```. В результате должны получить похожий результат: 
```
Apache Maven 3.6.0 (97c98ec64a1fdfee7767ce5ffb20918da4f719f3; 2018-10-24T21:41:47+03:00)
Maven home: /usr/local/Cellar/maven/3.6.0/libexec
Java version: 1.8.0_152, vendor: Oracle Corporation, runtime: /Library/Java/JavaVirtualMachines/jdk1.8.0_152.jdk/Contents/Home/jre
Default locale: en_GB, platform encoding: UTF-8
OS name: "mac os x", version: "10.14.2", arch: "x86_64", family: "mac"
```

#### Сборка проекта
Выполняем ```mvn package``` или  ```mvn install```. Первая команда скомпилирует ваш проект и 
поместит его в папку target, а вторая еще и положит его к вам в локальный репозиторий. 

#### Зависимости
Все популярные библиотеки находятся в центральном репозитории - https://mvnrepository.com/repos/central, поэтому их можно прописывать сразу в раздел dependencies вашего pom-файла. 
Например чтобы подключить Spring Framework необходимо определить следующую зависимость:
```
<dependencies>
      ...
      <dependency>
         <groupId>org.springframework</groupId>
         <artifactId>spring</artifactId>
         <version>2.5.5</version>
      </dependency>
   </dependencies>
```

Но необходимой библиотеки в центральном репозитории может и не быть, в этом случае надо прописать требуемый репозиторий вручную
в файле settings.xml, который лежит в корне вашего локального репозитория, либо в самом файле pom.xml, в зависимости от того, 
нужен ли данный репозиторий во всех проектах, либо в каком-то одном конкретном, соответственно. Пример для JBoss RichFaces:
```
<!-- JBoss RichFaces Repository -->
   <repositories>
      <repository>
         <releases>
            <enabled>true</enabled>
         </releases>
         <snapshots>
            <enabled>false</enabled>
            <updatePolicy>never</updatePolicy>
         </snapshots>
         <id>repository.jboss.com</id>
         <name>Jboss Repository for Maven</name>
         <url>
            http://repository.jboss.com/maven2/
         </url>
         <layout>default</layout>
      </repository>
   </repositories>
```
#### Плагины
Так как плагины являются такими же артефактами, как и зависимости, то они описываются практически так же. 
Вместо раздела dependencies – plugins, dependency – plugin, repositories – pluginRepositories, repository – pluginRepository.

Плагинами Maven делает все, даже непосредственно то, для чего он затевался – сборку проекта, только этот плагин необязательно 
указывать в свойствах проекта, если вы не хотите добавить какие-то фичи.

NB. В среде разработки IDEA Maven уже предустановлен.

#### Запуск тестов под Maven.
Берём проект с предыдвщего занятия - https://javarush.ru/groups/posts/605-junit - и пробуем запустить тесты на Maven.
Сделать это можно командой ```mvn -Dtest=TestName test```


#### Запуск тестов
Запустим тесты через чистый Maven: ```mvn -Dtest=ApplicationTest test```

Запуск целой сьюты: ```mvn -Dtest=MainSuiteTest test```

#### Пример pom.xml
 - custom project structure
 - custom java version

```
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>ru.mipt.examples</groupId>
    <artifactId>WordCount</artifactId>
    <version>1.0</version>

    <dependencies>
        <dependency>
            <groupId>org.apache.hadoop</groupId>
            <artifactId>hadoop-common</artifactId>
            <version>2.7.3</version>
        </dependency>
        <dependency>
            <groupId>org.apache.hadoop</groupId>
            <artifactId>hadoop-mapreduce-client-core</artifactId>
            <version>2.7.3</version>
        </dependency>
    </dependencies>

    <build>
    <sourceDirectory>${project.basedir}/src</sourceDirectory>
    <plugins>
        <plugin>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>2.3.2</version>
            <configuration>
                <source>1.7</source>
                <target>1.7</target>
            </configuration>
        </plugin>
    </plugins>
    </build>

</project>

```